# Configuration Management with Puppet and Terraform By Chetan

## Introduction

Integrating Terraform with Puppet allows organizations to combine the infrastructure provisioning capabilities of Terraform with the configuration management capabilities of Puppet, enabling end-to-end automation of infrastructure deployment and configuration.

## Prerequisites

Before starting with Terraform and puppet, we must have the following things:

- **Terraform**: 
- **Account on Cloud Service Providers:** (eg.aws , azure):
- **Puppet Server or Agent:** 


## Installation Steps

### Install Terraform

1. Download and install Terraform on your local machine.
2. Add the Terraform executable to your system's PATH.

### Set Up AWS Credentials

1. Configure your AWS credentials using the AWS CLI or environment variables. Ensure your IAM user has the necessary permissions to create EC2 instances, VPCs, subnets, and security groups.

### Prepare Puppet Master EC2 Instance

1. Launch an EC2 instance using the provided Terraform script. This instance will serve as the Puppet Master.
2. Save the private key (forGitLab.pem) in a secure location.


## Configuration Details

**1. Terraform Configuration:**
Create a Terraform configuration to provision infrastructure components. Here's an example Terraform configuration to provision an AWS EC2 instance
![alt text](screenshot/teraform1.png)



**2. Puppet Manifests:**
Write Puppet manifests to configure the provisioned infrastructure components. Here's an example Puppet manifest to install and configure a package on the provisioned instance
![alt text](screenshot/manifest.png)



**3. Integration**
To integrate Terraform with Puppet, we have a few options:

- Use Terraform provisioners (remote-exec, local-exec, etc.) to trigger Puppet runs on the provisioned instances.
- Utilize configuration management tools like Ansible or Shell scripts within Terraform to execute Puppet runs remotely on the provisioned instances.

Let's go with first options
Here is the example :
![alt text](screenshot/direct.png)
or
![alt text](screenshot/main.png)



## Running The Integration 

- Open Command prompt or terminal and go to terraform folder `cd Terraform`

- Edit the credentials as per your AWS account like ami id, keypair, etc.

- Run the following commands

    `terraform init`: Initializes a Terraform working directory by downloading providers and modules.

    `terraform validate`: Checks the configuration for syntax errors and other issues.

    `terraform plan`: Creates an execution plan, showing changes to be applied to infrastructure.

    `terraform apply`: Applies changes to the infrastructure as described in the execution plan.

- After completing work `terraform destroy`: Destroys the Terraform-managed infrastructure.

## Testing 
See screenshots how it will look


- Aws ec2 Instance running :
![alt text](screenshot/EC2_instance.png)

- puppetagent
![alt text](screenshot/puppetagent_run.png)

- puppetmaster
![alt text](screenshot/puppetserverrun.png)

- After the terraform apply it triggers the puppet client to run

![alt text](screenshot/nginx.png)


## Contributor
[https://gitlab.com/GitChetan/] - chetan Patil - WDGET2024014# Puppet_and_Terraform 
