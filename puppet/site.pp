node default{
    package {'nginx':
        ensure => installed,
    }

    file {'/tmp/Trial.txt':
        content => 'Nginx Installed',
        mode => '0644',
    }

    service {'nginx':
        ensure =>running,
        enable => true,
    }
}