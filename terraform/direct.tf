provider "aws" {
    region = "us-east-1"
}

resource "aws_instance" "Puppet_master" {
    ami = "ami-07d9b9ddc6cd8dd30"
    instance_type = "t2.micro"
    key_name = "puppet"
    tags = {
      Name = "Puppet_master"
    }

    provisioner "remote-exec" {
       inline=[
        "sudo /opt/puppetlabs/puppet/bin/puppet agent -t --server=puppetmaster.local",
       ]
       connection {
            type        = "ssh"
            user        = "ubuntu"
            private_key = file("/Users/chetanpatil/Desktop/code/puppet.pem")
            host        = aws_instance.puppetMaster.public_ip
            agent       = true
        }
    }
}